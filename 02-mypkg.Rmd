# mypkg

Mon nouveau package sur git.

## Objectifs

  * Git : initier un projet
  * Commandes git : remote add, push --set-upstream, branch, merge, pull

## 1) Initier un projet git

* 1.1 Sur Gitlab, créer un nouveau projet publique vide

* 1.2 En local, créer un nouveau package R avec au choix : 

  * Naviguer en clique-bouton dans la création d'un nouveau projet (avec d'un nouveau répertoire/dossier, un package R utilisant devtools)
  
  * Exécuter `usethis::create_package(path = "")` avec le chemin voulu dans le terminal (avec le nom du package `mypkg`)

* 1.3 S'assurer que git est utilisé pour ce projet (cliquer sur le ficheir `mypkg.Rproj` et naviguer dans Git/SNV).

* 1.4 S'assurer que `Roxygen` est utilisé pour compiler la documentation du package.

* 1.5 S'assurer que la compilation du package se fait par installation et redémarage de la session R.

* 1.6 Compiler le package vide.

* 1.7 Ajouter une fonction `stat_fun` de calcul de statistiques descriptives au package.

* 1.8 Compiler et faire un `check` du package.

* 1.9 Versionner les changements effectués

```{r}
git add .
git commit -m ":baby: first commit"
```

* 1.10 Pousser les changements vers le serveur Gitlab distant (aussi appelé remote) en adaptant l'adresse du serveur distant (`remote`).

> Ce serveur distant est (très) souvent appelé `origin`. Dans un premier temps nous déclarons ce nouveau serveur distant, le nommons et donnons sont adresse.

```{r}
git remote add origin git@gitlab.com:cbo14142/mypkg.git
```

> Dans un deuxième temps nous poussons notre répertoire git et déclarons la synchronisation entre notre version du répertoire `master` et celle appelée aussi `master` sur le serveur distant `origin`

```{r}
git push --set-upstream origin master
```

> Ces commandes sont documentées dans tous les projets vides sur Gitlab. E.g : https://gitlab.com/cbo14142/mypkg

## 2) Travailler avec des branches

Nous allons à présent amener une évolution dans le projet avec l'inclusion de données. 

* 2.1 Créer une nouvelle branche (une nouvelle version) du projet, elle est souvent appelée `dev` :

```{r}
git branch dev
```

* 2.2 Vérifier la création de cette branche puis naviguer dans la branche dev avec :

```{r}
git checkout dev
```


* 2.3 Ajouter un répertoire de données brutes `data-raw` avec :

```{r}
usethis::use_data_raw()
```

* 2.4 Créer un fichier `mtcars2.R` dans `data-raw` qui créé le fichier `mtcars.csv` dans `data-raw` avec les noms de voitures comme variable et non nom de ligne.

* 2.5 Ajouter ces données à l'utilisation du package

```{r}
?usethis::use_data
```

* 2.6 Faire un commit puis un push de la nouvelle branche

> Suivre les instructions de git pour résoudre le problème

## 3) Réunir les travaux avec `merge`

* 3.1 Se positionner en `master`

* 3.2 Effectuer un merge de `dev` dans `master`

```{r}
git merge dev
```

> Comme les changements ont été effectué uniquement en dev il n'y a pas de conflits
> C'est également le cas quand les travaux sont répartis entre les devs dans des branches séparées, sur des fichiers/dossiers distincts

## 4) Gérer les conflits

* 4.1 Créer un conflit en éditant `stat_fun.R` en master et dev avec :

* master

```{r}
# un commentaire depuis master
```


* dev

```{r}
# un commentaire depuis dev
```

* 4.2 Commiter les changements. Faire un merge de `dev` dans `master`

* 4.3 Régler les conflits

* 4.4 Uniformiser les branches

* 4.5 Régler les conflits avec Gitlab

  * 4.5.1 Créer 2 nouveaux conflits de la forme "# un 2eme commentaire depuis master"

  * 4.5.2 Pousser ces changements sur `remote`
  
  * 4.5.3 Créer une "Merge request" pour merger `dev` en `master`
  
  * 4.5.4 Importer les changements de `remote` sur le répertoire git local
  
```{r}
git pull
```
   
  * 4.5.5 Vérifier et corriger le merge si besoin (avec commit et push)
  
  * 4.5.6 Uniformiser les branches

## 5) Travailler avec  les`Issues` Gitlab

Les "tickets" (`Issues`) des projets Gitlab permettent d'historiser l'évolution d'un projet, les bugs et les nouvelles fonctions. E.g un ticket nommé "Ajout d'un panel de visualisation de la production" peut être affecté à un (des) développeur(s), avec un objectif de temps déterminé, comme partie d'une évolution majeure du projet (`Milestones`).

Créer des `Issues` permet également de rattacher des commits à un objectif plus général. On peut notamment clore une issue avec le message de commit tel que "closes #1 fix the data source" où "closes #1" est le strict nécessaire pour clore l'`Issue` et le reste ce qui documente le commit.

* 5.1 Créer un ticket "Ajouter un README"

* 5.2 Le résoudre